package learning.google.codejam.c2009.qualifier;

import learning.google.codejam.c2009.qualifer.WelcomeToCodeJam;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WelcomeToCodeJamTest {
	
	@Test
	public void testGoogle() {
		test(1, "elcomew elcome to code jam");
		test(256, "wweellccoommee to code qps jam");
		test(0, "welcome to codejam");
	}
	
	public void test(int expected, String text) {
		test(expected, text, "welcome to code jam");
	}
	
	public void test(int expected, String text, String sentence) {
		assertEquals(expected, new WelcomeToCodeJam(text, sentence).solve());
	}
}
