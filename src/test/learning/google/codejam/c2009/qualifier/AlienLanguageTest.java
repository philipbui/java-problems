package learning.google.codejam.c2009.qualifier;

import learning.google.codejam.c2009.qualifer.AlienLanguage;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class AlienLanguageTest {
	
	@Test
	public void googleTest() {
		Set<String> dict = new HashSet<>(Arrays.asList("abc",
				"bca",
				"dac",
				"dbc",
				"cba"));
		test(2, dict, "(ab)(bc)(ca)");
		test(1, dict, "abc");
		
	}
	public void test(int expected, Set<String> dict, String s) {
		assertEquals(expected, new AlienLanguage(dict, s).solve());
	}
}
