package learning.google.codejam.c2009.r1a;

import learning.google.codejam.c2017.r1a.AlphabetCake;
import org.junit.Test;

public class AlphabetCakeTest {
	
	@Test
	public void googleTest() {
		/*
		char[][] cake = serialize("G??\n" +
				"?C?\n" +
				"??J", 3, 3);
		new AlphabetCake(cake, 3, 3).solve();
		*/
		char[][] cake2 = serialize("CODE\n" +
				"????\n" +
				"?JAM", 3, 4);
		new AlphabetCake(cake2, 3, 4).solve();
	}
	
	private char[][] serialize(String s, int r, int c) {
		char[][] cake = new char[r][c];			
		String[] lines = s.split("\n");
		for (int i = 0; i < lines.length; i++) {
			for (int j = 0; j < lines[i].length(); j++) {
				cake[i][j] = lines[i].charAt(j);
			}
		}
		return cake;
	}
}
