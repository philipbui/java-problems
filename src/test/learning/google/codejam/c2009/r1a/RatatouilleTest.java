package learning.google.codejam.c2009.r1a;

import learning.google.codejam.c2017.r1a.Ratatouille;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RatatouilleTest {
	
	@Test
	public void googleTest() {
		test(1, 2, 1, new int[] {500, 300}, new int[][] {
				{
					500
				},
				{
					300
				}
		});
	}
	
	public void test(int expected, int n, int p, int[] ingredients, int[][] packages) {
		assertEquals(expected, new Ratatouille(n, p, ingredients, packages).solve());
	}
}
