package learning.google.codejam.c2017.r2a;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Steed2CruiseControlTest {
	
	@Test
	public void myTest() throws Exception {
		test(1.5, 21, 2, new int[][] {
				{4, 2},
				{7, 1}
		}, 1);
	}
	
	@Test
	public void googleTest() {
		test(101, 2525, 1, new int[][] {
				{2400, 5}
		}, 1);
		test(100, 300, 2, new int[][] {
				{120, 60},
				{60, 90}
		}, 1);
		test(33.333333, 100, 2, new int[][] {
				{80, 100},
				{70, 10}
		}, 6);
	}
	
	public void test(double expected, int d, int n, int[][] horses, int delta) {
		assertEquals(expected, new Steed2CruiseControl(d, n, horses).solve(), delta);
	}
}
