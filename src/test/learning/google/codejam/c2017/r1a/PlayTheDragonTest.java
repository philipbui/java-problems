package learning.google.codejam.c2017.r1a;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlayTheDragonTest {
	
	@Test
	public void googleTest() throws Exception {
		test(5, 11, 5, 16, 5, 0, 0);
	}
	
	@Test
	public void survivabilityTest() throws Exception {
		//assertNull("Didn't return null when 6-0 >= 5", new PlayTheDragon(5, 1, 3, 6, 1, 1).getSurvivableRound());
		//assertNull("Didn't return null when 6-1 >= 5", new PlayTheDragon(5, 1, 3, 6, 1, 1).getSurvivableRound());
		//assertEquals("3 0 2", new PlayTheDragon(5, 1, 3, 4, 1, 2).getSurvivableRound().toString());
		//assertEquals("1 9 1", new PlayTheDragon(10, 1, 3, 11, 11, 2).getSurvivableRound().toString());
		//assertEquals("23 11 0", new PlayTheDragon(23, 1, 3, 11, 11, 2).getSurvivableRound().toString());
	}
	
	public void test(int expected, int hd, int ad, int hk, int ak, int b, int d) throws PlayTheDragon.OutOfHealthException {
		assertEquals(expected, new PlayTheDragon(hd, ad, hk, ak, b, d).solve());
	}
}
