package learning.google.codejam.c2017.quality;

import learning.google.codejam.c2017.qualifer.BathroomStalls;
import learning.utils.IOUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BathroomStallsTest {
	
	private final String directory = "google/codejam/c2017/qualifier/";
	
	@Test
	public void testGoogle() {
		test(1, 0, 4, 2);
		test(1, 0, 5, 2);
		test(1, 1, 6, 2);
	}
	
	@Test
	public void testGoogleSmallPractice1() {
		List<String> tests = IOUtils.read(this.getClass().getResourceAsStream("C-small-practice-1.in"));
		
	}
	
	@Test
	public void testYoutubeGarauvSen() {
		test(1, 0, 6, 2);
	}
	
	public void test(int max, int min, int n, int k) {
		BathroomStalls bathroomStalls = new BathroomStalls(n, k);
		String actual = bathroomStalls.solve();
		assertEquals(n + ":" + k + " = " + Arrays.toString(bathroomStalls.occupied), max + " " + min, actual);
	}
}
