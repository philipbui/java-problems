package learning.collections.tree;

import org.junit.Test;

import java.util.Comparator;

import static org.junit.Assert.assertEquals;

public class HeapTest {
	
	@Test
	public void hackerRankTest() {
		Heap<Integer> heap = new Heap<>(Comparator.naturalOrder());
		heap.add(10);
		heap.add(15);
		heap.add(20);
		heap.add(17);
		test(10, heap);
		heap.add(8);
		test(8, heap);
		heap.add(25);
		test(8, heap);
	}
	
	public void test(int expected, Heap<Integer> heap) {
		assertEquals(Integer.valueOf(expected), heap.peek());
	}
}
