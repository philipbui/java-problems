package learning.utils;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathUtilsTest {

	@Test
	public void test() {
		Assert.assertEquals(MathUtils.factorial(9), 362880);
	}
}

