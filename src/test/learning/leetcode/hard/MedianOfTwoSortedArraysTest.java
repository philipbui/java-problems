package learning.leetcode.hard;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MedianOfTwoSortedArraysTest {
	
	@Test
	public void testLeetCode() {
		test(2.0, new int[] {1, 3}, new int[] {2});
		test(2.5, new int[] {1, 2}, new int[] {3, 4});
	}
	
	public void test(double expected, int[] nums1, int[] nums2) {
		Assert.assertEquals(expected, new MedianOfTwoSortedArrays().find(nums1, nums2), 2);
	}
}
