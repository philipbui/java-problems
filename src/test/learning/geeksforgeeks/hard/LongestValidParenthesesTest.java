package learning.geeksforgeeks.hard;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LongestValidParenthesesTest {
	
	@Test
	public void geeksForGeeksTest() {
		testLength("((()", 2);
		testLength(")()())", 4);
		testLength("()(()))))", 6);
	}
	
	private void testLength(String s, int expected) {
		int actual = new LongestValidParentheses().maxLength(s);
		assertEquals("Error parsing " + s + ". Found " + actual + ", expected " + expected, expected, actual);
	}
}
