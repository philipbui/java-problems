package learning.geeksforgeeks.hard;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class NthDigitOfPiTest {
	
	@Test
	public void testAllDigits() {
		String pi = "31415926535897932384626433832795028841971693993751";
		for (int i = 0, n = pi.length(); i < n; i++) {
			test(Integer.parseInt(pi.substring(i, i+1)), i+1);
		}
		
	}
	
	public void test(int expected, int digit) {
		assertEquals("Digit " + digit + " did not match with " + expected, expected, new NthDigitOfPi().nthDigit(digit));
	}
}
