package learning.geeksforgeeks.hard;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BooleanParenthesizationTest {
	
	@Test
	public void geekForGeeksTest() {
		test("T|T&F^T", 4);
		test("T^F|F", 2);
	}
	
	public void test(String s, int expected) {
		Assert.assertEquals(expected, new BooleanParenthesization(s).numberOfSolutions());
	}
}
