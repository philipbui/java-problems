package learning.geeksforgeeks.hard;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DistinctPalindromicSubstringsTest {
	
	@Test
	public void testSimple() {
		DistinctPalindromicSubstrings object = new DistinctPalindromicSubstrings("aba");
		object.solvePermutations();
	}
	
	@Test
	public void testContinous() {
		testContinous("abaaa", 5);
		testContinous("geek", 4);
	}
	
	public void testContinous(String s, int expected) {
		assertEquals(expected, new DistinctPalindromicSubstrings(s).solveContinuous().size());
	}
}
