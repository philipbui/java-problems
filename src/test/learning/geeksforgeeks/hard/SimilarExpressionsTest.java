package learning.geeksforgeeks.hard;

import org.junit.Test;

import static learning.geeksforgeeks.hard.SimilarExpressions.compare;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SimilarExpressionsTest {
	
	@Test
	public void test() {
		assertTrue(compare("-a-b-c", "-(a+b+c)"));
		assertFalse(compare("a-b-(c-d)", "a-b-c-d"));
		assertTrue(compare("(-(-(-(-(a+b+c))))", "a+b+c"));
	}
	
	@Test
	public void testSameCharacterAddMinus() {
		success("a-a+b+c", "(c+b)");
	}
	
	@Test
	public void testMultiScopeNegative() {
		success("-(a-b+c)-(-d+e)", "-a+b-c+d-e");
	}
	
	@Test
	public void testEmpty() {
		success("-a+a", "");
		success("a-a", "");
		fail("a", "");
	}
	
	private void success(String a, String b) {
		assertTrue(compare(a, b));
	}
	
	private void fail(String a, String b) {
		assertFalse(compare(a, b));
	}
}
