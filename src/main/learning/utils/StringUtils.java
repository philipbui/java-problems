package learning.utils;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {
	
	/**
	 * The number of strings in a power-set of S is 2^n. Representing this as bits, 1 representing on and 0 representing off.
	 * a,b,c = 8.
	 * 0,0,0 = 
	 * 0,0,1 = c
	 * 0,1,0 = b
	 * 1,1,0 = ab
	 * 1,1,1 = abc
	 * @param s String
	 * @return List<String>
	 */
	public static List<String> powerSet(String s) {
		long bits = (1L << s.length()) - 1; // Bit-shift left characters count so 1L bit-shift left 3 characters become 1000 - 1 to 0111
		return powerSet(s, bits, new ArrayList<>());  
	}
	
	/**
	 * Recursive function to find PowerSet of S given each Bit represents a character within S.
	 * Decrementing by one ensures we reach each variation of Bits. E.g 111(7) to 110(6) to 101(5) to 100(4) to 011(3).
	 * Tail recursive.
	 * @param s String
	 * @param bits Long representing Bits. Each bit representing a character in s
	 * @return 
	 */
	private static List<String> powerSet(String s, long bits, List<String> powerSet) {
		// End of recursion is met.
		if (bits < 0) {
			return powerSet;
		}
		
		StringBuilder subset = new StringBuilder();
		for (int i = 0, n = s.length(); i < n; i++) {
			if (BitUtils.isSet(bits, i)) {
				subset.append(s.charAt(i));
			}
		}
		powerSet.add(subset.toString());
		return powerSet(s, bits - 1, powerSet);
	}
}
