package learning.utils;

class MathUtils {
	
	static long factorial(int n) {
		if (n == 0) {
			return 1;
		}
		return n * (factorial(n-1));
	}
}
