package learning.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class IOUtils {
	
	public static List<String> read(InputStream in) {
		List<String> strings = new ArrayList<>();
		try (BufferedReader r = new BufferedReader(new InputStreamReader(in))) {
			String line;
			while ((line = r.readLine()) != null) {
				strings.add(line);
			}
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}
		return strings;
	}
}
