package learning.utils;

/**
 * Bitwise &. When both bits are 1, return 1. Else return 0.
 */
public class BitUtils {
	
	/**
	 * If end bits both end in 1, bitwise & will return a 1. (All left 1s return 0)
	 * Example: 1101 & 0001 = 0001
	 * @param bits Natural Number
	 * @return True if odd
	 */
	public static boolean isOdd(int bits) {
		return (bits & 1) == 1;
	}
	
	/**
	 * If end bit ends in 0, bitwise & will return 0.  (All left 1s return 0)
	 * Example: 1010 & 0001 = 0000 
	 * @param bits Natural Number
	 * @return True if even
	 */
	public static boolean isEven(int bits) {
		return (bits & 1) == 0;
	}
	
	/**
	 * Bitwise & operation between Bits and a Bit with a single 1.
	 * A bitwise & operation will return a non-zero if both bits at i are 1.
	 * Example: 1011 & 0010 = 0010. 1011 & 0100 = 0000.
	 * See https://en.wikipedia.org/wiki/Bitwise_operation#AND
	 * @param bits Long representing Bits
	 * @param i ith index starting from 0.
	 * @return True if ith Bit is set.
	 */
	public static boolean isSet(long bits, int i) {
		return (bits & (1 << i)) != 0; // Bit shift left
	}
}
