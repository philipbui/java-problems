// you can also use imports, for example:
import java.util.*;

// you can write to stdout for debugging purposes, e.g.
// System.out.println("this is a debug message");

class Calcium {
	
	public final int MAX_DISTANCE = 900;
	public int diameter[];
	public int subtreeDiameter[];
	public final int maxNodes = 50001;
	
	class Node {
		public int id;
		public List<Integer> edgeList;
		
		public Node(int id) {
			this.id = id;
			edgeList = new ArrayList<Integer>();
		}
		
		public void addEdge(int edge) {
			edgeList.add(edge);
		}
	}
	
	public int solution(int[] A, int[] B, int K) {
		Node[] nodes = createTree(A, B);
		diameter = new int[maxNodes];
		subtreeDiameter = new int[maxNodes];
		int first = 0;
		int last = Math.min(A.length, MAX_DISTANCE);
		int minStreetDistance = Integer.MAX_VALUE;
		int mid;
		while (first <= last) {
			mid = (first + last) / 2;
			Arrays.fill(diameter, -2);
			diameter[0] = -1;
			if (splitTree(nodes, nodes[0], mid) <= K) {
				minStreetDistance = mid;
				last = mid - 1;
			} else {
				first = mid + 1;
			}
		}
		return minStreetDistance;
	}
	
	public Node[] createTree(int[] A, int[] B) {
		Node[] nodes = new Node[maxNodes];
		for (int i = 0; i < A.length; i++) {
			if (nodes[A[i]] == null) {
				nodes[A[i]] = new Node(A[i]);
			}
			if (nodes[B[i]] == null) {
				nodes[B[i]] = new Node(B[i]);
			}
			nodes[A[i]].addEdge(B[i]);
			nodes[B[i]].addEdge(A[i]);
		}
		return nodes;
	}
	
	public int splitTree(Node[] nodes, Node node, int distanceLimit) {
		int cameras = 0;
		for (int nodeId: node.edgeList) {
			if (diameter[nodeId] == -2) { // Not visited, mark as visit and visit it
				diameter[nodeId] = -1;
				cameras += splitTree(nodes, nodes[nodeId], distanceLimit);
			}
		}
		int maxDiameter = -1;
		int subNodes = 0;
		for (int nodeId : node.edgeList) {
			if (diameter[nodeId] >= 0) {
				subtreeDiameter[subNodes] = (diameter[nodeId]);
				subNodes++;
			}
		}
		Arrays.sort(subtreeDiameter, 0, subNodes);
		for (int i = 0; i < subNodes / 2; i++) {
			int temp = subtreeDiameter[i];
			subtreeDiameter[i] = subtreeDiameter[subNodes - 1 - i];
			subtreeDiameter[subNodes - 1 - i] = temp;
		}
		for (int i = 0; i < subNodes; i++) {
			if (i != subNodes - 1 && subtreeDiameter[i] + subtreeDiameter[i+1] + 2 > distanceLimit) {
				cameras++;
			}
			else if (i == subNodes - 1 && subtreeDiameter[i] + 1 > distanceLimit) {
				cameras++;
			}
			else {
				maxDiameter = Math.max(maxDiameter, subtreeDiameter[i]);
			}
		}
		if (maxDiameter == -1) { //  We removed all subtrees, diameter is 0 since not linked to tree
			diameter[node.id] = 0;
		}
		else { // Diameter at this node is max diameter of its biggest subtree + 1
			diameter[node.id] = maxDiameter + 1;
		}
		return cameras;
	}
}