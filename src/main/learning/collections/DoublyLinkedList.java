package learning.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Doubly Linked List where Nodes point to both previous and next Nodes.
 * Used for learning, use Javas LinkedList for a optimized version.
 * The Java version ensures 
 * @param <E> Generic
 */
public class DoublyLinkedList<E> implements List<E> {
	
	private Node<E> head;
	private Node<E> tail;
	private int size;
	
	public DoublyLinkedList() {
		
	}
	
	/**
	 * Links e as first element.
	 */
	private void linkFirst(E e) {
		Node<E> prevHead = head;
		head = new Node<>(e, null, prevHead);
		if (prevHead != null) {
			prevHead.prev = head;
		} else { // First element, set tail to head as well.
			tail = head;
		}
		size++;
	}
	
	/**
	 * Links e as last element.
	 */
	private void linkLast(E e) {
		Node<E> prevTail = tail;
		tail = new Node<>(e, null, prevTail);
		if (prevTail != null) {
			prevTail.next = tail;
		} else { // First element, set head to tail as well.
			head = tail;
		}
		size++;
	}
	
	/**
	 * (1) Head = 1. Tail = 1.
	 * 1.Prev = null. Head = 1.Next(null).
	 * 1.Next = null. Tail = 1.Prev(null).
	 * <p>
	 * (1,2) Head = 1. Tail = 1. Remove 2.
	 * 2.Prev = 1. Head not change. 1.Next = 2.Next(null)
	 * 2.Next = null. Tail = 2.Prev(1)
	 * <p>
	 * (1,3,5) Head = 1. Tail = 5. Remove 3.
	 * 3.Prev = 1. Head not change. 1.Next = 3.Next(5).
	 * 3.Next = 5. Tail not change. 5.Prev = 3.Prev(1)
	 *
	 * @param node Node<E>
	 */
	protected void unlink(Node<E> node) {
		if (node.prev != null) {
			node.prev.next = node.next;
		} else {
			head = node.next; // If Node has no previous, was head. Move next or null to Head.
		}
		if (node.next != null) {
			node.next.prev = node.prev;
		} else {
			tail = node.prev; // If node has no next, was tail. Move previous to Tail.
		}
		size--;
	}
	
	private Node<E> node(int index) {
		if (index < (size >> 1)) { // Index < Half of size (Bit-shift right 1 equivalent to /2^1).
			Node<E> next = head;
			while (index > 0) {
				//for (int i = 0; i < index; i++) {
				next = next.next;
				index --;
			}
			return next;
		} else {
			Node<E> prev = tail; // There is a tail if index > half of size.
			while (index < size - 1) {
				//for (int i = size - 1; i > index; i--) {
				prev = prev.prev;
				index ++;
			}
			return prev;
		}
	}
	
	@Override
	public int size() {
		return size;
	}
	
	@Override
	public boolean isEmpty() {
		return head == null;
	}
	
	@Override
	public boolean contains(Object o) {
		for (Node node = head; node != null; node = node.next) {
			if (node.equals(o)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new ListItr();
	}
	
	@Override
	public Object[] toArray() {
		// TODO:
		return new Object[0];
	}
	
	@Override
	public <T> T[] toArray(T[] a) {
		// TODO:
		return a;
	}
	
	@Override
	public boolean add(E e) {
		linkLast(e);
		return true;
	}
	
	@Override
	public boolean remove(Object o) {
		for (Node<E> node = head; node != null; node = node.next) {
			if (node.equals(o)) {
				unlink(node);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public boolean addAll(Collection<? extends E> c) {
		return false;
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends E> c) {
		return false;
	}
	
	@Override
	public boolean removeAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		return false;
	}
	
	@Override
	public void clear() {
		head = null;
		tail = null;
	}
	
	private boolean isElementIndex(int index) {
		return index >= 0 && index < size;
	}
	
	@Override
	public E get(int index) {
		if (!isElementIndex(index)) {
			throw new IndexOutOfBoundsException();
		}
		
		return node(index).item;
	}
	
	@Override
	public E set(int index, E element) {
		//TODO:
		return null;
	}
	
	@Override
	public void add(int index, E element) {
		//TODO:
	}
	
	@Override
	public E remove(int index) {
		Node<E> node = node(index);
		unlink(node);
		return node.item;
	}
	
	@Override
	public int indexOf(Object o) {
		Node node = head;
		int i = 0;
		while (node != null) {
			node = node.next;
			i++;
		}
		return i -= 1;
	}
	
	@Override
	public int lastIndexOf(Object o) {
		Node node = tail;
		int i = size - 1;
		while (node != null) {
			node = node.prev;
			i--;
		}
		return i += 1;
	}
	
	@Override
	public ListIterator<E> listIterator() {
		return new ListItr();
	}
	
	@Override
	public ListIterator<E> listIterator(int index) {
		// TODO:
		return new ListItr();
	}
	
	@Override
	public List<E> subList(int fromIndex, int toIndex) {
		//TODO:
		return null;
	}
	
	public ListIterator<Node<E>> nodeListIterator() {
		return new ListNodeItr();
	}
	
	private class ListItr implements ListIterator<E> {
		
		@Override
		public boolean hasNext() {
			return false;
		}
		
		@Override
		public E next() {
			return null;
		}
		
		@Override
		public boolean hasPrevious() {
			return false;
		}
		
		@Override
		public E previous() {
			return null;
		}
		
		@Override
		public int nextIndex() {
			return 0;
		}
		
		@Override
		public int previousIndex() {
			return 0;
		}
		
		@Override
		public void remove() {
			
		}
		
		@Override
		public void set(E e) {
			
		}
		
		@Override
		public void add(E e) {
			
		}
	}
	
	private class ListNodeItr implements ListIterator<Node<E>> {
		
		private Node<E> prev;
		private Node<E> next;
		private int nextIndex;
		
		public ListNodeItr() {
			next = head;
		}
		
		@Override
		public boolean hasNext() {
			return next != null;
		}
		
		@Override
		public Node<E> next() {
			prev = next;
			next = next.next;
			nextIndex++;
			return next;
		}
		
		@Override
		public boolean hasPrevious() {
			return prev != null;
		}
		
		@Override
		public Node<E> previous() {
			next = prev;
			prev = prev.prev;
			nextIndex--;
			return prev;
		}
		
		@Override
		public int nextIndex() {
			return nextIndex;
		}
		
		@Override
		public int previousIndex() {
			return nextIndex - 1;
		}
		
		@Override
		public void remove() {
			if (prev != null) {
				if (prev.prev != null) {
					prev = prev.prev;
					unlink(prev.next);
				} else {
					unlink(prev);
					prev = null;
				}
				nextIndex--;
			} else {
				throw new IllegalStateException(); // Did not iterate once to remove.
			}
		}
		
		@Override
		public void set(Node<E> node) {
			//TODO:
		}
		
		@Override
		public void add(Node<E> node) {
			//TODO:
		}
	}
	
	public static class Node<E> {
		
		final E item;
		Node<E> prev, next;
		
		private Node(E item, Node<E> prev, Node<E> next) {
			this.item = item;
			this.prev = prev;
			this.next = next;
		}
	}
}
