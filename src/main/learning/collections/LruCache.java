package learning.collections;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Lru Cache. Uses a Map of Key -> LinkedListNode Values and a Doubly Linked List.
 * 
 * When an entry is inserted, insert key and node value to both map and beginning of queue.
 * When an entry is removed, remove key from map and unlink node from queue.
 * When an entry is gotten, extract value from node.
 * When cache limit hits, remove entry from beginning of the queue (O(1) DoublyLinkedList).
 * @param <K>
 * @param <V>
 */

public class LruCache<K, V> implements Map<K, V> {
	
	private final Map<K, DoublyLinkedList.Node<V>> cache;
	private final DoublyLinkedList<V> queue;
	public LruCache() {
		cache = new HashMap<>();
		queue = new DoublyLinkedList<V>();
	}
	
	@Override
	public int size() {
		return cache.size();
	}
	
	@Override
	public boolean isEmpty() {
		return cache.isEmpty();
	}
	
	@Override
	public boolean containsKey(Object key) {
		return cache.containsKey(key);
	}
	
	@Override
	public boolean containsValue(Object value) {
		//noinspection SuspiciousMethodCalls
		return queue.contains(value);
	}
	
	@Override
	public V get(Object key) {
		DoublyLinkedList.Node<V> v = cache.get(key);
		if (v != null) {
			queue.unlink(v);
			
		} else { // Cache miss
			return null;
		}
		return null;
	}
	
	@Override
	public V put(K key, V value) {
		//if ()
		return null;
	}
	
	@Override
	public V remove(Object key) {
		DoublyLinkedList.Node<V> node = cache.remove(key);
		if (node != null) {
			queue.unlink(node);
			return node.item;
		} else {
			return null;
		}
	}
	
	@Override
	public void putAll(Map<? extends K, ? extends V> m) {
		
	}
	
	@Override
	public void clear() {
		cache.clear();
		queue.clear();
	}
	
	@Override
	public Set<K> keySet() {
		return cache.keySet();
	}
	
	@Override
	public Collection<V> values() {
		return queue.subList(0, queue.size());
	}
	
	@Override
	public Set<Entry<K, V>> entrySet() {
		return null;
	}
}
