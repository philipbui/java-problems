package learning.collections;

public class MakingAnagrams {
	
	public static int numberNeeded(String first, String second) {
		int[] chars = new int[26];
		for (char c: first.toCharArray()) {
			chars[c-'a'] += 1;
		}
		for (char c: second.toCharArray()) {
			chars[c-'a'] -= 1;
		}
		int sum = 0;
		for (int count: chars) {
			sum += Math.abs(count);
		}
		return sum;
	}
	
}
