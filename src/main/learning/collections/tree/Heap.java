package learning.collections.tree;

import java.util.Arrays;
import java.util.Comparator;

public class Heap<E> {
	
	private E[] items; // Array of Items. 
	private int capacity; // Length of Items.
	private int size; // Current slot of heap
	private Comparator<? super E> comparator;
	
	public Heap(Comparator<? super E> comparator) {
		this.comparator = comparator;
		capacity = 32;
		//noinspection unchecked
		items = (E[])new Object[capacity];
	}
	
	public E peek() {
		if (size == 0) throw new IllegalStateException();
		return items[0];
	}
	
	public E poll() {
		if (size == 0) throw new IllegalStateException();
		E item = items[0];
		swap(0, size - 1);
		size--;
		heapifyDown();
		return item;
	}
	
	public void add(E item) {
		ensureExtraCapacity();
		items[size] = item;
		size++;
		heapifyUp();
	}
	
	protected void a() {
		
	}
	
	private void heapifyUp() {
		int i = size - 1; // Get last element.
		while (hasParent(i) && comparator.compare(getParent(i), items[i]) > 0) {
			swap(getParentIndex(i), i);
			i = getParentIndex(i);
		}
	}
	
	private void heapifyDown() {
		int i = 0; // Get root / first element.
		// Since Heaps are always balanced, the left child will always be inserted before right child, 
		// hence no need to check for right child
		while (hasLeftChild(i)) { 
			int smallestChild = getLeftChildIndex(i);
			// If left child greater than right child
			if (comparator.compare(items[smallestChild], getRightChild(i)) > 0) {
				smallestChild = getRightChildIndex(i);
			}
			// If current node is greater than child, move down
			if (comparator.compare(items[i], items[smallestChild]) > 0) {
				swap(i, smallestChild);
				i = smallestChild;
			} else {
				break;
			}
		}
	}
	
	private int getLeftChildIndex(int i) {
		return (i << 1) + 1;
	}
	
	private int getRightChildIndex(int i) {
		return (i << 1) + 2;
	}
	
	private int getParentIndex(int i) {
		return (i - 1) >> 1;
	}
	
	private boolean hasLeftChild(int i) {
		return getLeftChildIndex(i) < size;
	}
	
	private boolean hasRightChild(int i) {
		return getRightChildIndex(i) < size;
	}
	
	/**
	 * Could be refactored to i > 0?
	 * @param i Int Index
	 * @return True
	 */
	private boolean hasParent(int i) {
		return getParentIndex(i) >= 0;
	}
	
	/**
	 * Assumes that hasLeftChild was called previously or an IndexOutOfBoundsException may be thrown.
	 * @param i
	 * @return
	 */
	private E getLeftChild(int i) {
		return items[getLeftChildIndex(i)];
	}
	
	/**
	 * Assumes that hasRightChild was called previously or an IndexOutOfBoundsException may be thrown.
	 * @param i
	 * @return
	 */
	private E getRightChild(int i) {
		return items[getRightChildIndex(i)];
	}
	
	/**
	 * Assumes that hasParent was called previously or an IndexOutOfBoundsException may be thrown.
	 * @param i
	 * @return
	 */
	private E getParent(int i) {
		return items[getParentIndex(i)];
	}
	
	private void swap(int i, int i2) {
		E tmp = items[i];
		items[i] = items[i2];
		items[i2] = tmp;
	}
	
	private void ensureExtraCapacity() {
		if (size == capacity) {
			capacity = capacity << 1;
			items = Arrays.copyOf(items, capacity);
		}
	}
}
