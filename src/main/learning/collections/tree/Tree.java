package learning.collections.tree;

import java.util.Comparator;

public abstract class Tree<E> {
	
	Node<E> root;
	final Comparator<? super E> comparator;
	
	Tree(Comparator<? super E> comparator) {
		this.comparator = comparator;
	}
	
	static class Node<E> {
		
		E data;
		Node<E> l, r;
		
		Node(E data) {
			this.data = data;
		}
	}
	
	abstract public Node<E> get(E data);
	abstract public void add(E data);
	abstract public boolean remove(E data);
}
