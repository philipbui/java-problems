package learning.collections.tree;

import java.util.ArrayDeque;
import java.util.Comparator;

public class BST<E> extends Tree<E> {
	
	BST(Comparator<? super E> comparator) {
		super(comparator);
	}
	
	@Override
	public Node<E> get(E data) {
		if (root == null) {
			return null;
		}
		ArrayDeque<Node<E>> stack = new ArrayDeque<>();
		stack.push(root);
		Node<E> node;
		while (!stack.isEmpty()) {
			node = stack.pop();
			int compare = comparator.compare(data, node.data);
			if (compare == 0) {
				return node;
			} else if (compare < 0 && node.l != null) { // Data < Node Data
				stack.push(node.l);
			} else if (compare > 0 && node.r != null) { // Data > Node Data
				stack.push(node.r);
			}  else {
				break;
			}
		}
		return null;
	}
	
	@Override
	public void add(E data) {
		if (root == null) {
			root = new Node<>(data);
			return;
		}
		ArrayDeque<Node<E>> stack = new ArrayDeque<>(1);
		Node<E> node = root;
		stack.push(node);
		int compare = 0;
		while (!stack.isEmpty()) {
			node = stack.pop();
			compare = comparator.compare(data, node.data);
			if (compare <= 0 && node.l != null) { // Data < Node Data
				stack.push(node.l);
			} else if (compare > 0 && node.r != null) { // Data > Node Data
				stack.push(node.r);
			} else {
				break;
			}
		}
		if (compare <= 0) { // While loop if statement failed so we can expect Node to be null
			node.l = new Node<>(data);
		} else {
			node.r = new Node<>(data);
		}
	}
	
	@Override
	public boolean remove(E data) {
		if (root == null) {
			return false;
		}
		ArrayDeque<Node<E>> stack = new ArrayDeque<>(1);
		Node<E> node = root;
		stack.push(node);
		Node<E> prev = null;
		int compare;
		boolean left = false;
		while (!stack.isEmpty()) {
			node = stack.pop();
			compare = comparator.compare(data, node.data);
			if (compare == 0) {
				remove(prev, node, left);
				return true;
			} else if (compare < 0 && node.l != null) { // Node data greater than data
				stack.push(node.l);
				left = true;
			} else if (compare > 0 && node.r != null) { // Node Data less than Data
				stack.push(node.r);
				left = false;
			} else {
				break;
			}
			prev = node;
		}
		return false;
	}
	
	private void remove(Node<E> prev, Node<E> node, boolean left) {
		if (node.l != null && node.r != null) { // Node has two childs, swap value with In-order successor Node.
			node.data = removeMinValueNode(node, node.r).data;
		} else if (prev == null) { // Root. The first if statement would have handled roots with two children.
			if (node.l != null) {
				root = node.l;
			} else if (node.r != null) {
				root = node.r;
			} else {
				root = null;
			}
		} else if (node.l == null && node.r == null) {
			if (left) {
				prev.l = null;
			} else {
				prev.r = null;
			}
		} else if (node.l != null) { // Node only has left child
			if (left) {
				prev.l = node.l;
			} else {
				prev.r = node.l;
			}
		} else { // Node only has right child.
			if (left) {
				prev.l = node.r;
			} else {
				prev.r = node.r;
			}
		}
	}
	
	private Node<E> removeMinValueNode(Node<E> prev, Node<E> node) {
		if (node.l != null) { // If node has left child
			prev = node;
			while (true) { // Get most left child
				node = node.l;
				if (node.l == null) {
					break;
				}
				prev = node;
			}
			remove(prev, node, true);
		} else { // Node is min value since no left child.
			remove(prev, node, false);
		}
		return node;
	}
}
