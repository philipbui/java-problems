package learning.collections.cache;

import java.util.*;

public class LfuCache<K, V> {
	
	private Map<K, Node<V>> cache;
	private List<LinkedHashSet<K>> frequency;
	private final int maxCapacity;
	
	public LfuCache(int maxCapacity) {
		this.maxCapacity = maxCapacity;
		cache = new HashMap<>();
		frequency = new ArrayList<>();
	}
	
	public V get(K key) {
		Node<V> node = cache.get(key);
		if (node != null) {
			removeFromFrequency(node.frequency, key);
			node.frequency++;
			addToFrequency(node.frequency, key);
			return node.value;
		} else {
			return null;
		}
	}
	
	public void set(K key, V value) {
		if (cache.size() >= maxCapacity) {
			 for (LinkedHashSet<K> frequencies: frequency) {
			 	if (frequencies != null && !frequencies.isEmpty()) {
			 		Iterator<K> iterator = frequencies.iterator();
			 		cache.remove(iterator.next());
			 		iterator.remove();
				}
			 }
		}
		cache.put(key, new Node<V>(value));
	}
	
	private void removeFromFrequency(int i, K key) {
		LinkedHashSet<K> frequencies = frequency.get(i);
		if (frequencies != null) {
			frequencies.remove(key);
		}
	}
	
	private void addToFrequency(int i, K key) {
		LinkedHashSet<K> frequencies = frequency.get(i);
		if (frequencies == null) {
			frequencies = new LinkedHashSet<>();
		}
		frequencies.add(key);
		frequency.add(i, frequencies);
	}
	
	public static class Node<V> {
		
		final V value;
		int frequency;
		
		public Node(V value) {
			this.value = value;
		}
	}
}
