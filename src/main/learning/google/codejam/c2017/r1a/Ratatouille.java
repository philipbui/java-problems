package learning.google.codejam.c2017.r1a;

import java.util.Arrays;

/**
 * https://code.google.com/codejam/contest/5304486/dashboard#s=p1
 */
public class Ratatouille {
	
	public final int n, p;
	public final int[] ingredients;
	public final int[] lower;
	public final int[] higher;
	public final int[][] packages;
	public Ratatouille(int n, int p, int[] ingredients, int[][] packages) {
		this.n = n;
		this.p = p;
		this.ingredients = ingredients;
		this.lower = new int[p];
		this.higher = new int[p];
		for (int i = 0; i < p; i++) {
			lower[i] = (int) (ingredients[i] * 0.9);
			higher[i] = (int) (ingredients[i] * 1.1);
		}
		this.packages = packages;
	}
	
	private boolean isValid(int[] currentPackage) {
		for (int i = 0; i < n; i++) {
			if (!isValid(i, currentPackage[i])) {
				return false;
			}
		}
		return true;
	}
	
	private boolean isValid(int i, int number) {
		if (number > lower[i]) {
			if (number > ingredients[i]) {
				number %= ingredients[i];
				number += ingredients[i];
			}
			return number >= lower[i] && number <= higher[i];
		}
		return false;
	}
	
	public void addPackage(int i, int[] currentPackage) {
		for (int j = 0; j < p; j++) {
			currentPackage[j] += packages[i][j];
		}
	} 
	
	public int solve() {
		return solve(new boolean[n], new int[n], 0);
	}
	
	public int solve(boolean[] choices, int[] currentPackage, int index) {
		int sum = 0;
		if (isValid(currentPackage)) {
			sum += 1;
			currentPackage = new int[n];
		}
		int max = 0;
		for (int i = index; i < n; i++) {
			if (!choices[i]) {
				boolean[] updatedChoices = Arrays.copyOf(choices, n);
				updatedChoices[i] = true;
				int[] updatedPackage = Arrays.copyOf(currentPackage, n);
				addPackage(i, updatedPackage);
				max = Math.max(max, solve(updatedChoices, updatedPackage, i+1));
			}
		}
		return sum;
	}
}
