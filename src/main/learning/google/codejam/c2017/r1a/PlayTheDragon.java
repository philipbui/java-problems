package learning.google.codejam.c2017.r1a;

public class PlayTheDragon {
	
	private final int healthDragon, attackDragon, healthKnight, attackKnight, buff, debuff;
	
	PlayTheDragon(int healthDragon, int attackDragon, int healthKnight, int attackKnight, int buff, int debuff) {
		this.healthDragon = healthDragon;
		this.attackDragon = attackDragon;
		this.healthKnight = healthKnight;
		this.attackKnight = attackKnight;
		this.buff = buff;
		this.debuff = debuff;
	}
	
	public int solve() throws OutOfHealthException {
		if (attackKnight >= healthDragon && attackDragon < healthKnight) { // If the knight can one shot us and we cannot.
			throw new OutOfHealthException();
		}
		int attackTurns = (int) Math.ceil(healthKnight / attackDragon);
		if (buff > 0) {
			int buffs = 0;
			while (true) {
				int turns = (int) Math.ceil(healthKnight / (attackDragon + buffs * buff));
				if (turns > attackTurns) {
					break;
				} else {
					attackTurns = turns;
					buffs++;
				}
			}
		}
		if (attackKnight <= 0 || attackTurns <= (int) Math.ceil(healthDragon / attackKnight)) {
			return attackTurns; // Can kill the knight before they kill us.
		}
		Round currentRound = getSurvivableRound();
		if (currentRound == null) { // Cannot survive or indefinitely.
			return 0;
		}
		int minTurns = Integer.MAX_VALUE;
		try {
			while (currentRound.attackKnight > 0) { // Calculate all types of choices with all de-buffs up to Knight Attack = 0.
				minTurns = Math.min(minTurns, currentRound.calculate(attackTurns, buff));
				currentRound.debuff(buff, debuff);
			}
			minTurns = Math.min(minTurns, currentRound.calculate(attackTurns, buff));
		} catch (OutOfHealthException e) {
			
		}
		return minTurns;
	}
	
	/**
	 * Min Turns of survivability is specified that the dragon should be able to survive a turn(attack) then perform healing again.
	 * @return Initial round if survivable, a new round with new stats and turn to start from if survivable after de-buffing, else null.
	 */
	Round getSurvivableRound() {
		if (healthDragon > attackKnight * 2) { // Can withstand two attacks minimum.
			return new Round(healthDragon, attackKnight, 0);
		} else {
			int attackKnight = this.attackKnight;
			int currentHealth = healthDragon;
			int turns = 0;
			while (currentHealth < attackKnight * 2) {
				// Can't de-buff. Next turn will die.
				if (currentHealth <= attackKnight - debuff) {
					if (buff <= attackKnight) {
						return null; // Cannot out-heal or indefinite healing.
					} else {
						//System.out.println("Healed health from " + currentHealth + " to " + (currentHealth + buff));
						currentHealth += buff;
					}
				} else {
					//System.out.println("Debuffed knight from " + attackKnight + " to " + (attackKnight - debuff));
					attackKnight -= debuff;
					System.out.println();
				}
				//System.out.println("Took damage from knight from " + currentHealth + " to " + (currentHealth - attackKnight));
				currentHealth -= attackKnight;
				turns++;
			}
			return new Round(currentHealth, attackKnight, turns);
		}
	}
	
	public static class Round {
		
		int healthDragon;
		int attackKnight;
		int turns;
		
		Round(int healthDragon, int attackKnight, int turns) {
			this.healthDragon = healthDragon;
			this.attackKnight = attackKnight;
			this.turns = turns;
		}
		
		@Override
		public String toString() {
			return healthDragon + " " + attackKnight + " " + turns;
		}
		
		void debuff(int buff, int debuff) throws OutOfHealthException {
			if (healthDragon <= attackKnight) {
				healthDragon += buff;
				healthDragon -= attackKnight;
				assert healthDragon > attackKnight;
				turns++;
			}
			attackKnight -= debuff;
			healthDragon -= attackKnight;
			turns++;
		}
		
		int calculate(int turns, int buff) throws OutOfHealthException {
			int totalTurns = this.turns;
			int healthDragon = this.healthDragon;
			int attackKnight = this.attackKnight;
			while (turns > 0) {
				if (healthDragon <= attackKnight) {
					healthDragon += buff;
					healthDragon -= attackKnight;
				} else {
					healthDragon -= attackKnight;
					turns--;
				}
				totalTurns ++;
			}
			return totalTurns;
		}
	}
	
	public static class OutOfHealthException extends Exception {
		
	}
}
