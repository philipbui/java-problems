package learning.google.codejam.c2017.r1a;

import java.util.Arrays;

@SuppressWarnings("ALL")
public class AlphabetCake {
	
	private final char[][] cake;
	private final int r, c;
	private final char e = '?';
	private int filled;
	public AlphabetCake(char[][] cake, int r, int c) {
		this.cake = cake;
		this.r = r;
		this.c = c;
	}
	
	public void solve() {
		int size = r * c;
		while (filled < size) {
			filled = 0;
			for (int i = 0; i < r; i++) {
				for (int j = 0; j < c; j++) {
					if (cake[i][j] == e) {
						char c;
						boolean canInsert;
						if ((c = getLeft(i, j)) != e) {
							if (getBottomLeft(i, j) == c && getBottom(i, j) != e) {
								
							} else if (getTopLeft(i, j) == c && getTop(i, j) != e) {
								
							} else {
								if (checkTop(c, i, j - 1, j) && checkBottom(c, i, j - 1, j)) {
									cake[i][j] = c;
									filled += 1 + insertUpwards(c, i, j - 1, j) + insertDownwards(c, i, j - 1, j);
									printArray();
								}
							}
						} 
						if ((c = getTop(i, j)) != e) {
							if (getTopLeft(i, j) == c && getLeft(i, j) != e) {
								
							} else if (getTopRight(i, j) == c && getRight(i, j) != e) {
								
							} else {
								if (checkLeft(c, j, i - 1, i) && checkRight(c, j, i - 1, i)) {
									cake[i][j] = c;
									filled += 1 + insertLeft(c, j, i - 1, i) + insertRight(c, j, i - 1, i);
									printArray();
								}
							}
						} 
						if ((c = getRight(i, j)) != e) {
							if (getTopRight(i, j) == c && getTop(i, j) != e) {
								
							} else if (getBottom(i, j) == c && getBottom(i, j) != e) {
								
							} else {
								if (checkTop(c, i, j + 1, j) && checkBottom(c, i, j + 1, j)) {
									cake[i][j] = c;
									filled += 1 + insertUpwards(c, i, j + 1, j) + insertDownwards(c, i, j + 1, j);
									printArray();
								}
							}
						} 
						if ((c = getBottom(i, j)) != e){
							if (getBottomLeft(i, j) == c && getLeft(i, j) != e) {
								
							} else if (getBottomRight(i, j) == c && getRight(i, j) != e) {
								
							} else {
								if (checkLeft(c, j, i + 1, i) && checkRight(c, j, i + 1, i)) {
									cake[i][j] = c;
									filled += 1 + insertLeft(c, j, i + 1, i) + insertRight(c, j, i + 1, i);
									printArray();
								}
							}
						}
					} else {
						filled++;
					}
				}
			}
		}
	}
	
	private int insertUpwards(char c, int i, int j, int emptyJ) {
		int sum = 0;
		while (hasTop(i)) {
			char jChar = cake[i][j];
			char emptyJChar = cake[i][emptyJ];
			if (jChar == c) {
				if (emptyJChar == e) {
					cake[i][emptyJ] = c;
					sum++;
				} else if (emptyJChar != c) {
					throw new IllegalStateException();
				}
			} else {
				break;
			}
			i--;
		}
		return sum;
	}
	
	private int insertDownwards(char c, int i, int j, int emptyJ) {
		int sum = 0;
		while (hasBottom(i)) {
			char jChar = cake[i][j];
			char emptyJChar = cake[i][emptyJ];
			if (jChar == c) {
				if (emptyJChar == e) {
					cake[i][emptyJ] = c;
					sum++;
				} else if (emptyJChar != c) {
					throw new IllegalStateException();
				}
			} else {
				break;
			}
			i++;
		}
		return sum;
	}
	
	private int insertLeft(char c, int j, int i, int emptyI) {
		int sum = 0;
		while (hasLeft(j)) {
			char iChar = cake[i][j];
			char emptyIChar = cake[emptyI][j];
			if (iChar == c) {
				if (emptyIChar == e) {
					cake[emptyI][j] = c;
					sum++;
				} else if (emptyIChar != c) {
					throw new IllegalStateException();
				}
			} else {
				break;
			}
			j--;
		}
		return sum;
	}
	
	private int insertRight(char c, int j, int i, int emptyI) {
		int sum = 0;
		while (hasRight(j)) {
			char iChar = cake[i][j];
			char emptyIChar = cake[emptyI][j];
			if (iChar == c) {
				if (emptyIChar == e) {
					cake[emptyI][j] = c;
					sum++;
				} else if (emptyIChar != c) {
					throw new IllegalStateException();
				}
			} else {
				break;
			}
			j++;
		}
		return sum;
	}
	
	private boolean checkTop(char c, int i, int j, int emptyJ) {
		while (i >= 0) {
			char jChar = cake[i][j];
			char emptyJChar = cake[i][emptyJ];
			if (jChar == e && emptyJChar == e)  { // Both tops are empty.
				break;
			} else if (jChar != c) { // Left char stopped.
				break;
			} else if (emptyJChar != e && jChar != emptyJChar) { // 
				return false;
			} 
			i--;
		}
		return true;
	}
	
	private boolean checkBottom(char c, int i, int j, int emptyJ) {
		while (i < r) {
			char jChar = cake[i][j];
			char emptyJChar = cake[i][emptyJ];
			if (jChar == e && emptyJChar == e)  { // Both tops are empty.
				break;
			} else if (jChar != c) { // Left char stopped.
				break;
			} else if (emptyJChar != e && jChar != emptyJChar) { // 
				return false;
			}
			i++;
		}
		return true;
	}
	
	private boolean checkLeft(char c, int j, int i, int emptyI) {
		while (j >= 0) {
			char iChar = cake[i][j];
			char emptyIChar = cake[emptyI][j];
			if (iChar == e && emptyIChar == e)  { // Both tops are empty.
				break;
			} else if (iChar != c) { // Left char stopped.
				break;
			} else if (emptyIChar != e && iChar != emptyIChar) { // 
				return false;
			}
			j--;
		}
		return true;
	}
	
	private boolean checkRight(char c, int j, int i, int emptyI) {
		while (j < this.c) {
			char iChar = cake[i][j];
			char emptyIChar = cake[emptyI][j];
			if (iChar == e && emptyIChar == e)  { // Both tops are empty.
				break;
			} else if (iChar != c) { // Left char stopped.
				break;
			} else if (emptyIChar != e && iChar != emptyIChar) { // 
				return false;
			}
			j++;
		}
		return true;
	}
	
	private boolean hasTop(int i) {
		return i > 0;
	}
	
	private boolean hasLeft(int j) {
		return j > 0;
	}
	
	private boolean hasBottom(int i) {
		return i < r - 1;
	}
	
	private boolean hasRight(int j) {
		return j < c-1;
	}
	
	private char getLeft(int i, int j) {
		return hasLeft(j) ? cake[i][j-1] : e;
	}
	
	private char getTopLeft(int i, int j) {
		return hasTop(i) && hasLeft(j) ? cake[i-1][j-1] : e;
	}
	
	private char getTop(int i, int j) {
		return hasTop(i) ? cake[i-1][j] : e;
	}
	
	private char getTopRight(int i, int j) {
		return hasTop(i) && hasRight(j) ? cake[i-1][j+1] : e;
	}
	
	private char getRight(int i, int j) {
		return hasRight(j) ? cake[i][j+1] : e;
	}
	
	private char getBottomRight(int i, int j) {
		return hasBottom(i) && hasRight(j) ? cake[i+1][j+1] : e;
	}
	
	private char getBottom(int i, int j) {
		return hasBottom(i) ? cake[i+1][j] : e;
	}
	
	private char getBottomLeft(int i, int j) {
		return hasBottom(i) && hasLeft(j) ? cake[i+1][j-1] : e;
	}
	
	private void printArray() {
		for (int i = 0; i < r; i++) {
			System.out.println(Arrays.toString(cake[i]));
		}
	}
}
