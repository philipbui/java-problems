package learning.google.codejam.c2017.r2a;

public class StableNeighbours {
	
	public final int n;
	private int r,o,y,g,b,v;
	
	public StableNeighbours(int n, int r, int o, int y, int g, int b, int v) {
		this.n = n;
		this.r = r;
		this.o = o;
		this.y = y;
		this.g = g;
		this.b = b;
		this.v = v;
	}
	
	
}
