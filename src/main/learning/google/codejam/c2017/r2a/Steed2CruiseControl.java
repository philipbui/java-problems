package learning.google.codejam.c2017.r2a;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * https://code.google.com/codejam/contest/8294486/dashboard#s=p0
 * We assume that horse1 finishes the last since it will always match speed with any horse[2..n] it overlaps.
 * We also assume that horse1 does not increase speed ever, hence we can expect that Annie doesn't overlap with horse1 at
 * the crossing line, she will never overlap before-hand. E.g D = 21. Horse1 = 4,2. Horse2 = 7,1.
 * 6. 8. 10. 11. 12. 13. 14. 15. 16. 17. 18. 19. 20. 21. 14 Turns. (Note at Turn 3 H1 reduces speed to H1 to 1 since they overlap at 10).
 * 1.5 3 4.5 6. 7.5. 9. 10.5 12 13.5 15 16.5 18 19.5 21. Annie's optimal speed is 1.5 and never surpassed Horse 1.
 * A slight increase will overlap just before the finish line (the greater, the earlier you will overlap).
 * Thus we are only concerned that Annie never crosses paths with horse1 until the finish line.
 * Answer is total distance / (Time it took H1 position to cross line) for Annie's fastest speed to never cross H1.
 */
public class Steed2CruiseControl {
	
	// D = Destination distance KM, N = Number of horses.
	@SuppressWarnings("FieldCanBeLocal")
	private final int d, n;
	private final Horse[] horses;
	
	Steed2CruiseControl(int d, int n, int[][] horses) {
		assert d > 0; // Must be a distance greater than 0.
		assert n > 0; // Must be at least one other horse, or Annie can go infinite speed.
		this.d = d;
		this.n = n;
		this.horses = new Horse[n];
		for (int i = 0; i < n; i++) {
			assert horses[i][0] < d; // Horse position must be less than distance.
			this.horses[i] = new Horse(i, horses[i][0], horses[i][1]);
		}
	}
	
	public double solve() {
		PriorityQueue<Horse> ithHorse = new PriorityQueue<>((o1, o2) -> {
			return (int) (o1.k - o2.k); // If horse1 K >, returns 1.
		});
		ithHorse.addAll(Arrays.asList(horses));
		Horse horse1 = ithHorse.poll();
		double horse1Time = 0;
		while (!ithHorse.isEmpty()) {
			Horse nextHorse = ithHorse.poll();
			// If not collide, horse[nextHorse] is faster and skipped.
			// It could collide with horses next in line, but 
			if (horse1.willCollide(nextHorse, d)) {
				horse1Time += horse1.collide(nextHorse);
			}
		}
		horse1Time += (d - horse1.k) / horse1.s; // Trod through remaining distance at horse1 current(possibly lowered) speed.
		return d / horse1Time;
	}
	
	public static class Horse {
		
		// K = Initial Kilometers Position, S = KMph
		private int horseNumber;
		private double k;
		private int s;
		
		Horse(int horseNumber, double k, int s) {
			this.horseNumber = horseNumber;
			this.k = k;
			this.s = s;
		}
		
		double time(int d) {
			//System.out.println(this + " Time to " + d + ":" + (d - k) / s);
			return (d - k) / s;
		}
		
		/**
		 * Checks if current speed > horse speed and current time < horse time.
		 *
		 * @param horse Another horse.
		 * @param d     Total distance.
		 * @return True if this horse will collide or overlap before race finished.
		 */
		boolean willCollide(Horse horse, int d) {
			return time(d) < horse.time(d);
		}
		
		/**
		 * A given object at time T is object(time 0) + velocity * time.
		 * h1(t)=h1(0)+v*t
		 * h2(t)=h2(0)+v*t
		 * H1 5,2 H2 10,1. Will collide after 5 turns.
		 * H1 5,2 H2 11,1. Will collide after 6 turns. H1. 17. H2. 17.
		 * H1 5,2 H2 12,1. Will collide after 7 turns. H1. 19. H2. 19.
		 * H1 5,2 H2 12,0.5. Will collide after (12-5) / (2-0.5) = 7 / 1.5 = 4.66666666667 turns.
		 * 5 + 2 * 4.6 = 14.3333333333
		 * 12 + 1 * 4.6 = 14.3333333333
		 * Hence T = (H2k - H1K) / (H2S - H1S).
		 *
		 * @param horse Horse which will collide
		 * @return Time taken.
		 */
		double collide(Horse horse) {
			// The next horse has speed greater and distance greater. Will never collide.
			// Or the next horse has speed lower and distance lower. Will never collide.
			if (horse.s >= s || horse.k < k) {
				throw new IllegalArgumentException(this + " and " + horse + " will never collide");
			}
			double t = (Math.abs(horse.k - k) / Math.abs(horse.s - s));
			//System.out.println(this + " and " + horse + " collides at " + t);
			k = k + s * t;
			s = Math.min(horse.s, s);
			return t;
		}
		
		@Override
		public String toString() {
			return "Horse " + horseNumber + " K:" + k + " S:" + s;
		}
	}
}
