package learning.google.codejam.c2017.qualifer;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * A certain bathroom has N + 2 stalls in a single row; the stalls on the left and right ends are permanently occupied by the bathroom guards. The other N stalls are for users.
 * Whenever someone enters the bathroom, they try to choose a stall that is as far from other people as possible. To avoid confusion, they follow deterministic rules: For each empty stall S, they compute two values LS and RS, each of which is the number of empty stalls between S and the closest occupied stall to the left or right, respectively. Then they consider the set of stalls with the farthest closest neighbor, that is, those S for which min(LS, RS) is maximal. If there is only one such stall, they choose it; otherwise, they choose the one among those where max(LS, RS) is maximal. If there are still multiple tied stalls, they choose the leftmost stall among those.
 * K people are about to enter the bathroom; each one will choose their stall before the next arrives. Nobody will ever leave.
 * When the last person chooses their stall S, what will the values of max(LS, RS) and min(LS, RS) be?
 */
public class BathroomStalls {
	
	private final int n, k;
	public boolean[] occupied;
	public BathroomStalls(int n, int k) {
		this.n = n; // N stalls + 2.
		this.k = k; // 
	}
	
	public String solve() {
		PriorityQueue<Space> spaces = new PriorityQueue<>((o1, o2) -> o2.size() - o1.size());
		occupied = new boolean[n + 2];
		occupied[0] = true;
		occupied[n+1] = true;
		spaces.add(new Space(0, n+1));
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		int remaining = k;
		int mid = 0;
		while (remaining > 0) {
			Space space = spaces.poll();
			mid = space.mid();
			spaces.add(new Space(space.start, mid));
			spaces.add(new Space(mid, space.end));
			occupied[mid] = true;
			remaining--;
		}
		int left = 0;
		for (int i = mid - 1; i >= 0 && !occupied[i]; i--) {
			left++;
		}
		int right = 0;
		for (int j = mid + 1; j < n + 2 && !occupied[j]; j++) {
			right++;
		}
		return Math.max(left, right) + " " + Math.min(left, right); 
	}
	
	public static class Space {
		final int start, end;
		
		Space(int start, int end) {
			this.start = start;
			this.end = end;
		}
		
		public int size() {
			return end - start;
		}
		
		public int mid() {
			int mid = start + end;
			if ((mid & 1) == 1) { // If odd
				return mid / 2;
			} else {
				return mid / 2;
			}
		}
	}
	
	/* Old solution
	public void solve() {
		OccupiedStall[] stalls = new OccupiedStall[n + 2]; // We consider null stalls as not occupied.
		stalls[0] = new OccupiedStall(0, n + 1);
		stalls[n + 1] = new OccupiedStall(0, n + 1);
		int remainingPeople = k;
		
		while (remainingPeople > 0) {
			remainingPeople--;
		}
	}
	
	public static class OccupiedStall {
		private int left, right;
		
		OccupiedStall(int left, int right) {
			this.left = left;
			this.right = right;
		}
	}*/
}
