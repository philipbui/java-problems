package learning.google.codejam.c2009.qualifer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AlienLanguage {
	
	private final Set<String> dict;
	private char[] answers;
	private char[][] answerChoices;
	private int[] choiceIndexes;
	
	public AlienLanguage(Set<String> dict, String s) {
		this.dict = dict;
		List<Character> answers = new ArrayList<>();
		List<List<Character>> answerChoices = new ArrayList<>();
		List<Character> choices = null;
		List<Integer> choiceIndexes = new ArrayList<>();
		int chars = 0;
		for (char c: s.toCharArray()) {
			switch (c) {
				case '(':
					choices = new ArrayList<>(10);
					break;
				case ')':
					assert choices != null;
					answerChoices.add(choices);
					choices = null;
					answers.add('\u0000');
					choiceIndexes.add(chars);
					chars++;
					break;
				default:
					if (choices == null) {
						answers.add(c);
						chars++;
					} else {
						choices.add(c);
					}
					break;
			}
		}
		this.answers = new char[answers.size()];
		for (int i = 0, n = answers.size(); i < n; i++) {
			this.answers[i] = answers.get(i);
		}
		this.answerChoices = new char[answerChoices.size()][];
		for (int i = 0, n = answerChoices.size(); i < n; i++) {
			choices = answerChoices.get(i);
			this.answerChoices[i] = new char[choices.size()];
			for (int j = 0, n2 = choices.size(); j < n2; j++) {
				this.answerChoices[i][j] = choices.get(j);
			}
		}
		this.choiceIndexes = new int[choiceIndexes.size()];
		for (int i = 0, n = choiceIndexes.size(); i < n; i++) {
			this.choiceIndexes[i] = choiceIndexes.get(i);
		}
	}
	
	public int solve() {
		return solve(0);
	}
	
	private int solve(int i) {
		if (i >= choiceIndexes.length) {
			if (dict.contains(new String(answers))) {
				return 1;
			} else {
				return 0;
			}
		}
		int sum = 0;
		for (int j = 0, n = answerChoices[i].length; j < n; j++) {
			answers[choiceIndexes[i]] = answerChoices[i][j];
			sum += solve(i + 1);
		}
		return sum;
	}
}
