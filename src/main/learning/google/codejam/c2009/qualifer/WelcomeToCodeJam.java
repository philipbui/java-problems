package learning.google.codejam.c2009.qualifer;

public class WelcomeToCodeJam {
	
	public final char[] s;
	private final char[] s2;
	
	public WelcomeToCodeJam(String s, String s2) {
		this.s = s.toCharArray();
		this.s2 = s2.toCharArray();
	}
	
	public int solve() {
		return solve(0, 0);
	}
	
	private int solve(int i, int j) {
		if (j == s2.length) { // Ended a sentence.
			return 1;
		}
		int sum = 0;
		while (i < s.length && j < s2.length) {
			if (s[i] == s2[j]) {
				sum += solve(i+1, j+1);
			}
			i++;
		}
		return sum;
	}
}
