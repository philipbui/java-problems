package learning.leetcode.hard;

class MedianOfTwoSortedArrays {
	
	double find(int[] nums1, int[] nums2) {
		int n = nums1.length;
		int m = nums2.length;
		int size = n + m;
		int middle = size / 2 + 1;
		int i = 0, j = 0, k = 0;
		int[] sorted = new int[middle + 1];
		while (k < middle && i < n && j < m) {
			if (nums1[i] <= nums2[j]) {
				sorted[k] = nums1[i];
				i++;
				k++;
			} else {
				sorted[k] = nums2[j];
				j++;
				k++;
			}
		}
		if (k < middle) { // Either i >= n || j >= m
			while (i < n) {
				sorted[k] = nums1[i];
				i++;
				k++;
			}
			while (j < m) {
				sorted[k] = nums2[j];
				j++;
				k++;
			}
		}
		return (double) ((size & 1) == 1 ? sorted[middle-1] : (sorted[middle-1] + sorted[middle-2])) / 2;
	}
}
