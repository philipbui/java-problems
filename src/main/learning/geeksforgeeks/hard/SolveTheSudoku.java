package learning.geeksforgeeks.hard;

import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

import static learning.geeksforgeeks.hard.SolveTheSudoku.SudokuSolvable.Type.SQUARE;
import static learning.geeksforgeeks.hard.SolveTheSudoku.SudokuSolvable.Type.X;
import static learning.geeksforgeeks.hard.SolveTheSudoku.SudokuSolvable.Type.Y;

public class SolveTheSudoku {
	
	// List of Candidate Sets.
	private final Set<Integer>[] ys;
	private final Set<Integer>[] xs;
	private final Set<Integer>[] squares;
	private int[][] mat;
	private boolean[][] prev;
	private final PriorityQueue<SudokuSolvable> solvableAreas;
	private final PriorityQueue<Coordinate> solvableCoordinates;
	
	@SuppressWarnings("unchecked")
	public SolveTheSudoku() {
		ys = new Set[9];
		fill(ys);
		xs = new Set[9];
		fill(xs);
		squares = new Set[9];
		fill(squares);
		solvableAreas = new PriorityQueue<>((o1, o2) -> {
			if (o1.data.size() == o2.data.size()) {
				return 0;
			} else if (o1.data.size() > o2.data.size()) {
				return 1;
			} else {
				return -1;
			}
		});
		solvableCoordinates = new PriorityQueue<>((o1, o2) -> {
			if (o1.possibleCandidates == o2.possibleCandidates) {
				return 0;
			} else if (o1.possibleCandidates > o2.possibleCandidates) {
				return 1;
			} else {
				return -1;
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	public SolveTheSudoku(int[][] mat) {
		this();
		prepare(mat);
	}
	
	private void prepare(int[][] mat) {
		this.mat = mat;
		for (int y = 0; y < 9; y++) {
			int ySquare = getSquare(y);
			for (int x = 0; x < 9; x++) {
				remove(y, x, ySquare, getSquare(x));
			}
		}
	}
	
	private void remove(int y, int x, int ySquare, int xSquare) {
		ys[y].remove(mat[y][x]);
		xs[x].remove(mat[y][x]);
		int square = xSquare + (ySquare*3);
		squares[square].remove(mat[y][x]);
		if (xs[x].size() == 1 || ys[y].size() == 1 || getSquare(xSquare, ySquare).size() == 1) {
			solve(y, x, ySquare, xSquare);
		}
	}
	
	
	
	/**
	 *
	 */
	@SuppressWarnings({"unchecked", "Duplicates"})
	public void solve() {
		solvableAreas.clear();
		for (int x = 0; x < 9; x++) {
			solvableAreas.add(new SudokuSolvable(X, x, xs[x]));
		}
		for (int y = 0; y < 9; y++) {
			solvableAreas.add(new SudokuSolvable(Y, y, ys[y]));
		}
		for (int sq = 0; sq < 9; sq++) {
			solvableAreas.add(new SudokuSolvable(SQUARE, sq, squares[sq]));
		}
		
		while (!solvableAreas.isEmpty()) {
			SudokuSolvable sudokuSolvable = solvableAreas.poll();
			switch (sudokuSolvable.type) {
				case SQUARE:
					int xUpper;
					int yUpper;
					if (sudokuSolvable.metadata < 3) {
						xUpper = sudokuSolvable.metadata * 3 + 3;
						yUpper = 3;
					} else if (sudokuSolvable.metadata < 6) {
						xUpper = (sudokuSolvable.metadata - 3) * 3 + 3;
						yUpper = 6;
					} else {
						xUpper = (sudokuSolvable.metadata - 6) * 3 + 3;
						yUpper = 9;
					}
					
					for (int x = xUpper - 3; x < xUpper; x++) {
						int xSquare = getSquare(x);
						for (int y = yUpper - 3; y < yUpper; y++) {
							if (mat[y][x] == 0) {
								solve(y, x, getSquare(y), xSquare);
							}
						}
					}
					break;
				case X:
					int x = sudokuSolvable.metadata;
					int xSquare = getSquare(x);
					for (int y = 0; y < 9; y++) {
						if (mat[y][x] == 0) {
							solve(y, x, getSquare(y), xSquare);
						}
					}
					break;
				case Y:
					int y = sudokuSolvable.metadata;
					int ySquare = getSquare(y);
					for (int x2 = 0; x2 < 9; x2++) {
						if (mat[y][x2] == 0) {
							solve(y, x2, ySquare, getSquare(x2));
						}
					}
					break;
			}
		}
		while (!solvableCoordinates.isEmpty()) {
			Coordinate coordinate = solvableCoordinates.poll();
			int y = coordinate.y;
			int x = coordinate.x;
			if (mat[y][x] == 0) {
				int ySquare = getSquare(y);
				int xSquare = getSquare(x);
				Set<Integer> candidates = getCandidates(y, x);
				if (candidates.size() == 1) {
					solve(y, x, ySquare, xSquare, candidates);
				} else {
					solveHidden(y, x, ySquare, xSquare, candidates);
				}
			}
		}
		printArray();
	}
	
	private void solve(int y, int x, int ySquare, int xSquare) {
		solve(y, x, ySquare, xSquare, getCandidates(y, x, ySquare, xSquare));
	}
	
	/**
	 * General solution for Naked Singles.
	 * @param y Y coordinate.
	 * @param x X Coordinate.
	 * @param ySquare Square of Y. See getSquare().
	 * @param xSquare Square of X. See getSquare().
	 * @param candidates Set of possible candidates.
	 */
	private void solve(int y, int x, int ySquare, int xSquare, Set<Integer> candidates) {
		if (mat[y][x] == 0) {
			if (candidates.size() == 1) {
				answer(candidates.iterator().next(), y, x, ySquare, xSquare);
			} else if (candidates.size() > 1) {
				solvableCoordinates.add(new Coordinate(x, y, candidates.size()));
			} else {
				throw new IllegalStateException();
			}
		}
	}
	
	private void solveHidden(int y, int x, int ySquare, int xSquare, Set<Integer> candidates) {
		int size = candidates.size();
		for (int xy = 0; xy < 9; xy++) {
			if (mat[y][xy] == 0) {
				candidates.removeAll(getCandidates(y, xy));
			}
			if (mat[xy][x] == 0) {
				candidates.removeAll(getCandidates(xy, x));
			}
			if (candidates.size() == 1) {
				answer(candidates.iterator().next(), y, x, ySquare, xSquare);
				return;
			}
		}
		solvableCoordinates.add(new Coordinate(x, y, size));
	}
	
	private void answer(int answer, int y, int x, int ySquare, int xSquare) {
		if (y == 3) {
			System.out.println();
		}
		mat[y][x] = answer;
		remove(y, x, ySquare, xSquare);
		printArray();
		System.out.println("[" + (x + 1) + "][" + (y + 1) + "] = " + answer + "\n");
	}
	
	static class Coordinate {
		private final int x, y;
		private final int possibleCandidates;
		Coordinate(int x, int y, int possibleCandidates) {
			this.x = x;
			this.y = y;
			this.possibleCandidates = possibleCandidates;
		}
	}
	
	static class SudokuSolvable {
		public enum Type {
			X, Y, SQUARE
		}
		private final Type type;
		private final int metadata;
		private final Set<Integer> data;
		
		SudokuSolvable(Type type, int metadata, Set<Integer> data) {
			this.type = type;
			this.metadata = metadata;
			this.data = data;
		}
	}
	
	private void fill(Set<Integer>[] sets) {
		for (int i = 0, i2 = sets.length; i < i2; i++) {
			sets[i] = new HashSet<>(9);
			for (int j = 1; j <= 9; j++) {
				sets[i].add(j);
			}
		}
	}
	
	private Set<Integer> getCandidates(int y, int x) {
		return getCandidates(y, x, getSquare(y), getSquare(x));
	}
	private Set<Integer> getCandidates(int y, int x, int ySquare, int xSquare) {
		Set<Integer> intersections = new HashSet<>(ys[y]);
		intersections.retainAll(xs[x]);
		intersections.retainAll(getSquare(xSquare, ySquare));
		return intersections;
	}
	
	/**
	 * xSquare = 0, ySquare = 0. 0 + (0*3) = 0.
	 * xSquare = 1. ySquare = 0. 1 + (0*3) = 1.
	 * xSquare = 2. ySquare = 1. 2 + (1*3) = 5.
	 * @param xSquare 0 - 2
	 * @param ySquare 0 - 2
	 * @return Square Number
	 */
	private Set<Integer> getSquare(int xSquare, int ySquare) {
		return squares[xSquare + (ySquare*3)];
	}
	
	private int getSquare(int i) {
		if (isLeft(i)) {
			return 0;
		} else if (isCenter(i)) {
			return 1;
		} else {
			return 2;
		}
	}
	
	private boolean isLeft(int i) {
		return i <= 2;
	}
	
	private boolean isCenter(int i) {
		return i >= 3 && i <= 5;
	}
	
	public boolean isRight(int i) {
		return i >= 6;
	}
	
	private static int step = 0;
	private void printArray() {
		step += 1;
		System.out.println("Step " + step);
		for (int i = 0; i < 9; i++) {
			System.out.println(Arrays.toString(mat[i]));
		}
	}
	
	/*
	private void printSquare(int square) {
		int xUpper;
		int yUpper;
		if (square < 3) {
			xUpper = square * 3 + 3;
			yUpper = 3;
		} else if (square < 6) {
			xUpper = (square - 3) * 3 + 3;
			yUpper = 6;
		} else {
			xUpper = (square - 6) * 3 + 3;
			yUpper = 9;
		}
	}*/
}
