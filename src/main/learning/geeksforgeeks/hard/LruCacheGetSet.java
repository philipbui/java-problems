package learning.geeksforgeeks.hard;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Queue;

public class LruCacheGetSet<K, V> {
	
	private final HashMap<K, V> cache;
	private final Queue<K> queue;
	private final int capacity;
	public LruCacheGetSet(int capacity) {
		cache = new HashMap<K, V>(capacity) {
			@Override
			public V put(K key, V value) {
				return super.put(key, value);
			}
		};
		queue = new ArrayDeque<K>(capacity);
		this.capacity = capacity;
	}
	
	public V get(K key) {
		return cache.get(key);
	}
	
	public void set(K key, V value) {
		if (cache.size() >= capacity) {
			while (!queue.isEmpty()) {
				if (cache.remove(queue.poll()) != null) {
					break;
				}
			}
		}
		cache.put(key, value);
	}
}
