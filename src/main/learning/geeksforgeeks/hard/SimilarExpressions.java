package learning.geeksforgeeks.hard;

public class SimilarExpressions {
	
	public static boolean compare(String a, String b) {
		int[] expressions = new int[26];
		countCharacters(a.toCharArray(), a.length(), 0, false, expressions);
		countCharacters(b.toCharArray(), b.length(),  0, true, expressions);
		for (int answer: expressions) {
			if (answer != 0) {
				return false;
			}
		}
		return true;
	}
	/**
	 * Year 7 Maths.
	 * +- == -. 
	 * -- = +.
	 * - = -.
	 * + = +.
	 * @param string Character array
	 * @param n Length of string
	 * @param i Current index of parsing
	 * @param isNegative Whether the scope is negative for arithmetic operations. See above table for what happens. 
	 *                   E.g 2+(-(1+2+3)) = -4 equivalent to 2-1-2-3 = -4.
	 *                   -2+(-(-(1+2+3))) = 4 equivalent to -2+1+2+3 = 4.
	 * @param expressions Value of reference to Array[26] to insert count of expressions
	 * @return
	 */
	private static int countCharacters(char[] string, int n, int i, boolean isNegative, int[] expressions) {
		int lastMinus = i - 2;
		while (i < n) {
			switch (string[i]) {
				case '-':
					if (lastMinus != i - 1) {  // Two negatives transform into a Plus.
						lastMinus = i;
					}
					break;
				case '+':
					break;
				case '(':
					if (i > 0 && string[i-1] == '-') {
						i = countCharacters(string, n, i + 1, !isNegative, expressions);
					} else {
						i = countCharacters(string, n, i + 1, isNegative, expressions);
					}
					break;
				case ')':
					return i;
				default:
					if (lastMinus == i - 1) {
						if (isNegative) { // Two negatives 
							System.out.println("Adding " + string[i]);
							expressions[string[i] - 'a'] += 1;
						} else {
							System.out.println("Subtracting " + string[i]);
							expressions[string[i] - 'a'] -= 1;
						}
					} else {
						if (isNegative) {
							System.out.println("Subtracting " + string[i]);
							expressions[string[i] - 'a'] -= 1;
						} else {
							System.out.println("Adding " + string[i]);
							expressions[string[i] - 'a'] += 1;
						}
					}
					break;
			}
			i+=1;
		}
		return i;
	}
}
