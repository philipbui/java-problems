package learning.geeksforgeeks.hard;

public class NthDigitOfPi {
	
	public NthDigitOfPi() {
		
	}
	
	public int nthDigit(int n) {
		if (n <= 1) {
			return 3;
		}
		double pi = Math.PI;
		double max = Double.MAX_VALUE / 100;
		while (n > 1) {
			n--;
			pi = pi * 10;
			if (pi > max) {
				pi %= 10;
			}
		}
		return (int) (pi % 10);
	}
}
