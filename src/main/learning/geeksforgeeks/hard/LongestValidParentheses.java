package learning.geeksforgeeks.hard;

/**
 * Characteristics of failure are
 * ) without an existing (
 * 
 */
public class LongestValidParentheses {
	
	public LongestValidParentheses() {
		
	}
	
	public int maxLength(String s) {
		int max = 0;
		int successes = 0;
		int left = 0;
		for (char c: s.toCharArray()) {
			switch (c) {
				case '(':
					left += 1;
					break;
				case ')':
					left -= 1;
					break;
				default:
					throw new IllegalArgumentException("Found " + c + ", expected only '(' and ')'");
			}
			if (left < 0) {
				left = 0;
				successes = 0;
			} else {
				successes += 1;
				max = Math.max(max, successes - left);
			}
		}
		return max;
	}
}
