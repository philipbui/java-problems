package learning.geeksforgeeks.hard;

import java.util.Stack;

public class FixedTwoNodesOfABst {
	
	public static class Node {
		int data;
		Node left, right;
		
		public Node(int data, Node left, Node right) {
			this.data = data;
			this.left = left;
			this.right = right;
		}
		
		private boolean isValid() {
			return (left == null || left.data < data) && 
					(right == null || right.data > data);
		}
	}
	
	private final Node root;
	
	public FixedTwoNodesOfABst(Node root) {
		this.root = root;
	}
	
	public Node[] solve() {
		Node[] problemNodes = new Node[4];
		Stack<Node> stack = new Stack<>();
		stack.push(root);
		Node next;
		Node prev = null;
		while (!stack.isEmpty()) {
			next = stack.pop();
			if (!next.isValid()) {
				if (problemNodes[0] == null) {
					problemNodes[0] = next;
					problemNodes[1] = prev;
				} else {
					problemNodes[2] = next;
					problemNodes[3] = prev;
					break;
				}
			}
			prev = next;
		}
		Node problem1 = problemNodes[0];
		Node parent1 = problemNodes[1];
		Node problem2 = problemNodes[2];
		Node parent2 = problemNodes[3];
		assert problem1 != null && parent1 != null && problem2 != null && parent2 != null;
		parentSwapChild(parent1, problem1, problem2); // Swap problem 1 and problem 2 for parent references.
		parentSwapChild(parent2, problem2, problem1);
		swap(problem1, problem2);
		return new Node[]{problem1, problem2};
	}
	
	private void parentSwapChild(Node parent, Node child, Node swap) {
		if (parent.left.equals(child)) {
			parent.left = swap;
		} else { // Switch parent references to other problem node.
			parent.right = swap;
		}
	}
	
	private void swap(Node o1, Node o2) {
		Node tmp = o1.left;
		o1.left = o2.left;
		o2.left = tmp;
		
		tmp = o1.right;
		o1.right = o2.right;
		o2.right = tmp;
	}
	
	@Override
	public String toString() {
		return "FixedTwoNodesOfABst{" +
				"root=" + root +
				'}';
	}
}
