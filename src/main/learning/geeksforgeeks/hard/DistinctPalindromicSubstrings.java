package learning.geeksforgeeks.hard;

import learning.utils.BitUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import learning.utils.StringUtils;

public class DistinctPalindromicSubstrings {
	
	private final String s;

	public DistinctPalindromicSubstrings(String s) {
		this.s = s;
	}
	
	public Set<String> solveContinuous() {
		Set<String> palindromes = new HashSet<>(s.length());
		palindromes.add(s.substring(0, 1));
		for (int i = 1, n = s.length(); i < n; i++) {
			palindromes.add(s.substring(i, i+1));
			if (s.charAt(i) == s.charAt(i-1)) {
				palindromes.addAll(solveContinous(i-1, i));
			}
			palindromes.addAll(solveContinous(i-1, i+1));
		}
		return palindromes;
	}
	
	private Set<String> solveContinous(int start, int end) {
		Set<String> palindromes = new HashSet<>();
		while (start >= 0 && end < s.length()) {
			if (s.charAt(start) == s.charAt(end)) {
				palindromes.add(s.substring(start, end+1));
				start--;
				end++;
			} else {
				break;
			}
		}
		return palindromes;
	}
	
	/**
	 * Finds all 
	 * @return
	 */
	public Set<String> solvePermutations() {
		int[] charCount = new int[26];
		Set<String> palindromes = new HashSet<>();
		for (char c: s.toCharArray()) {
			charCount[c - 'a'] += 1;
			palindromes.add(Character.toString(c)); // Single characters are also Palindromes.
		}
		// Middle characters represent the middle character of a given Palindrome.
		Set<Character> middleChars = new HashSet<>();
		// Choices represent the prefix and suffix of a given Palindrome.
		String choices = buildPalindromeChoices(charCount, middleChars);
		List<String> prefixSuffixes = StringUtils.powerSet(choices);
		StringBuilder reverseString;
		for (String prefixSuffix: prefixSuffixes) {
			reverseString = new StringBuilder(prefixSuffix);
			palindromes.add(reverseString.reverse().toString() + prefixSuffix);
			for (Character middleChar: middleChars) {
				reverseString = new StringBuilder(prefixSuffix);
				palindromes.add(reverseString.reverse().toString() + middleChar + prefixSuffix);
			}
		}
		return palindromes;
	}
	
	public long solutions() {
		int[] charCount = buildCharCount();
		long sum = 0;
		// Middle characters represent the middle character of a given Palindrome.
		Set<Character> middleChars = new HashSet<>();
		// Choices represent the prefix and suffix of a given Palindrome.
		String choices = buildPalindromeChoices(charCount, middleChars);
		
		//TODO: Use factorial with permutations to detect choices for each prefix suffix choice.
		return sum;
	}
	
	private int[] buildCharCount() {
		int[] charCount = new int[26];
		for (char c: s.toCharArray()) {
			charCount[c - 'a'] += 1;
		}
		return charCount;
	}
	
	private String buildPalindromeChoices(int[] charCount, Set<Character> middleChars) {
		StringBuilder choices = new StringBuilder();
		for (int i = 0; i < 26; i++) {
			int count = charCount[i];
			if (count > 0) {
				char ch = (char) (i + 'a');
				if (BitUtils.isOdd(count)) {
					middleChars.add(ch);
					count -= 1;
				}
				for (int c = 0, n = count / 2; c < n; c++) {
					choices.append(ch);
				}
			}
		}
		return choices.toString();
	}
}
