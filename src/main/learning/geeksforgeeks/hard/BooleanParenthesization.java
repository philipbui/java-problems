package learning.geeksforgeeks.hard;

import java.util.*;

import static learning.geeksforgeeks.hard.BooleanParenthesization.Node.Type.AND;
import static learning.geeksforgeeks.hard.BooleanParenthesization.Node.Type.OR;
import static learning.geeksforgeeks.hard.BooleanParenthesization.Node.Type.XOR;

public class BooleanParenthesization {
	
	private LinkedList<Node> booleans;
	private Set<String> expressions;
	
	public BooleanParenthesization(String s) {
		assert s != null && (s.charAt(0) == 'T' || s.charAt(1) == 'F');
		booleans = new LinkedList<>();
		expressions = new HashSet<>();
		Node currentNode = null;
		for (char c: s.toCharArray()) {
			switch (c) {
				case 'T':
					assert currentNode == null : "Invalid String passed, found two T or F";
					currentNode = new Node(true);
					break;
				case 'F':
					assert currentNode == null : "Invalid String passed, found two T or F";
					currentNode = new Node(false);
					break;
				case '&':
					assert currentNode != null  : "Invalid String passed, found & without previous T or F";
					currentNode.nextType = AND;
					booleans.add(currentNode);
					currentNode = null;
					break;
				case '|':
					assert currentNode != null  : "Invalid String passed, found | without previous T or F";
					currentNode.nextType = OR;
					booleans.add(currentNode);
					currentNode = null;
					break;
				case '^':
					assert currentNode != null  : "Invalid String passed, found ^ without previous T or F";
					currentNode.nextType = XOR;
					booleans.add(currentNode);
					currentNode = null;
					break;
				case ' ':
					break;
				default:
					throw new IllegalArgumentException("Invalid String passed, could not parse " + c);
			}
		}
		booleans.add(currentNode);
		//if (!isTrueExpression()) {
		//	throw new IllegalArgumentException("Default string did not evaluate to True");
		//}
	}
	
	public int numberOfSolutions() {
		numberOfSolutions(new LinkedList<>(booleans));
		return expressions.size();
	}
	
	private void numberOfSolutions(LinkedList<Node> booleans) {
		if (booleans.size() <= 2) {
			if (isTrueExpression()) {
				expressions.add(parseExpression(booleans));
			}
			return;
		}
		 
		for (int i = 1; i < booleans.size(); i++) {
			LinkedList<Node> booleanCopy = new LinkedList<>();
			booleanCopy.addAll(booleans);
			if (i > (booleanCopy.size() >> 1)) { // Greater than half.
				int index = booleanCopy.size();
				Node next = null, prev;
				ListIterator<Node> iter = booleanCopy.listIterator(booleanCopy.size());
				prev = iter.previous();
				while (index != i) {
					next = prev;
					prev = iter.previous();
					index --;
				}
				//System.out.println(prev.toString() + next);
				iter.remove();
				next.parenthesis(prev);
				//System.out.println("DESC" + parseExpression(booleanCopy));
			} else {
				int index = 0;
				Node prev = null, next;
				Iterator<Node> iter = booleanCopy.iterator();
				next = iter.next();
				while (index != i) {
					prev = next;
					next = iter.next();
					index ++;
				}
				//System.out.println(prev.toString() + next);
				iter.remove();
				next.parenthesis(prev);
				//System.out.println(parseExpression(booleanCopy));
			}
		}
	}
	
	private boolean isTrueExpression() {
		return parseExpression() != null;
	}
	
	private String parseExpression() {
		return parseExpression(booleans);
	}
	
	private String parseExpression(LinkedList<Node> booleans) {
		if (booleans.isEmpty()) {
			throw new IllegalStateException();
		}
		Iterator<Node> iter = booleans.iterator();
		Node prev = iter.next();
		StringBuilder expression = new StringBuilder();
		expression.append(prev.toString());
		while (iter.hasNext()) {
			Node next = iter.next();
			switch (prev.nextType) {
				case AND:
					// IF NOT TRUE TRUE
					if (!(prev.value && next.value)) {
						return null;
					} else {
						expression.append(next.toString());
					}
					break;
				case OR:
					// If FALSE FALSE
					if (!prev.value && !next.value) {
						return null;
					} else {
						expression.append(next.toString());
					}
					break;
				case XOR:
					// If FALSE FALSE or TRUE TRUE
					if ((!prev.value && !next.value) || (prev.value && next.value)) {
						return null;
					} else {
						expression.append(next.toString());
					}
					break;
				default:
					throw new IllegalStateException();	
			}
			prev = next;
		}
		return expression.toString();
	}
	
	@Override
	public String toString() {
		Iterator<Node> iter = booleans.iterator();
		Node prev = iter.next();
		StringBuilder expression = new StringBuilder();
		expression.append(prev.toString());
		while (iter.hasNext()) {
			Node next = iter.next();
			switch (prev.nextType) {
				case AND:
					expression.append(next.toString());
					break;
				case OR:
					expression.append(next.toString());
					break;
				case XOR:
					expression.append(next.toString());
					break;
				default:
					throw new IllegalStateException();
			}
			prev = next;
		}
		return expression.toString();
	}
	
	static class Node {
		
		boolean value;
		public enum Type {
			AND, OR, XOR;
			
			@Override
			public String toString() {
				switch (this) {
					case AND:
						return "&";
					case OR:
						return "|";
					case XOR:
						return "^";
					default:
						throw new IllegalStateException();
				}
			}
		}
		Type nextType;
		String previous;
		
		Node(boolean value) {
			this.value = value;
		}
		
		void parenthesis(Node prev) {
			System.out.print("Replacing " + prev + this);
			previous = "(" + prev.toString() + valueString() + ")";
			System.out.println(" with " + this);
			value = evaluate(prev);
		}
		
		boolean evaluate(Node prev) {
			switch (prev.nextType) {
				case AND:
					return prev.value && value;
				case OR:
					return prev.value || value;
				case XOR:
					return (prev.value && !value) || (!prev.value && value);
				default:
					throw new IllegalStateException();
			}
		}
		
		private char valueString() {
			return value ? 'T' : 'F';
		}
		
		@Override
		public String toString() {
			if (previous != null) {
				return previous + nextType;
			} else {
				return valueString() + (nextType != null ? nextType.toString() : "");
			}
		}
	}
}
