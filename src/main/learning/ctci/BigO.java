package learning.ctci;

public class BigO {
	
	public static void main(String[] args) {
		int[] ns = new int[]{30,
				1,
				4,
				9,
				16,
				25,
				36,
				49,
				64,
				81,
				100,
				121,
				144,
				169,
				196,
				225,
				256,
				289,
				324,
				361,
				400,
				441,
				484,
				529,
				576,
				625,
				676,
				729,
				784,
				841,
				907};
		for(int n: ns){
			if (isPrime(n)) {
				System.out.println("Prime");
			} else {
				System.out.println("Not prime");
			}
		}
	}
	
	public static boolean isPrime(int n) {
		if (n == 1) {
			return false;
		}
		double sqrt = Math.sqrt(n);
		for (int i = 2; i <= sqrt; i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}
}
