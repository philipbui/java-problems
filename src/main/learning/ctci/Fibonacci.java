package learning.ctci;

public class Fibonacci {
	
	public static int fibonacci(int n) {
		if (n <= 0) {
			return 0;
		} else if (n == 1) {
			return 1;
		}
		// Complete the function.
		int n2 = 1;
		int sum = 1;
		int prev = 0;
		int tmp;
		while (n2 < n) {
			tmp = sum;
			sum += prev;
			prev = tmp;
			n2++;
		}
		return sum;
	}
}
