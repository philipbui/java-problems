package learning.ctci;

import java.util.HashMap;
import java.util.Map;

public class CoinChange {
	
	public static void main(String[] args) {
		int[] coins = new int[] {2,5,3,6};
		int n = 10;
		int solutions = 0;
		Map<Integer, Integer> memoizations = new HashMap<>();
		for (int coin: coins) {
			solutions += numSolutions(n-coin, memoizations, coins);
		}
		System.out.print(solutions);
		//System.out.print(numSolutions(n, new HashMap<Integer, Integer>(), coins));
	}
	
	public static int numSolutions(int n, Map<Integer, Integer> memoizations, int[] coins) {
		if (memoizations.containsKey(n)) {
			return memoizations.get(n);
		}
		int defaultSolutions = 0;
		int solutions = 0;
		int sum;
		for (int coin: coins) {
			sum = n - coin;
			if (sum > 0) {
				defaultSolutions = Math.max(defaultSolutions, numSolutions(sum, memoizations, coins));
			} else if (sum == 0) {
				solutions += 1;
			}
		}
		
		memoizations.put(n, defaultSolutions + solutions);
		return defaultSolutions + solutions;
	}
}
