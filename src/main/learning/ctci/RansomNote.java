package learning.ctci;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RansomNote {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int m = in.nextInt();
		int n = in.nextInt();
		String magazine[] = new String[m];
		for(int magazine_i=0; magazine_i < m; magazine_i++){
			magazine[magazine_i] = in.next();
		}
		String ransom[] = new String[n];
		for(int ransom_i=0; ransom_i < n; ransom_i++){
			ransom[ransom_i] = in.next();
		}
		Map<String, Integer> words = new HashMap<>();
		for (String word: magazine) {
			words.compute(word, (k, v) -> (v == null) ? 1 : v + 1);
		}
		for (String word: ransom) {
			Integer count = words.get(word);
			if (count == null) {
				System.out.print("No");
				return;
			} else {
				count -= 1;
				if (count < 0) {
					System.out.print("No");
					return;
				}
				words.put(word, count);
			}
		}
		System.out.print("Yes");
	}
}
