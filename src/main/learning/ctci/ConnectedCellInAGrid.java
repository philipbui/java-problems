package learning.ctci;

import java.util.Scanner;
import java.util.Stack;

public class ConnectedCellInAGrid {
	
	@SuppressWarnings("Duplicates")
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int grid[][] = new int[n][m];
		for(int grid_i=0; grid_i < n; grid_i++){
			for(int grid_j=0; grid_j < m; grid_j++){
				grid[grid_i][grid_j] = in.nextInt();
			}
		}
		int max = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < m; j++) {
				if (grid[i][j] == 1) {
					int sum = 1;
					grid[i][j] = 0;
					Stack<Coordinate> stack = new Stack<>();
					stack.push(new Coordinate(i, j));
					Coordinate c = null;
					int x, y;
					while (!stack.isEmpty()) {
						c = stack.pop();
						x = c.x - 1;
						if (x >= 0) { // Left Hand Side
							y = c.y - 1;
							if (y >= 0 && grid[x][y] == 1) {
								sum += 1;
								grid[x][y] = 0;
								stack.push(new Coordinate(x, y));
							}
							y = c.y;
							if (grid[x][y] == 1) {
								sum += 1;
								grid[x][y] = 0;
								stack.push(new Coordinate(x, y));
							}
							y = c.y + 1;
							if (y < m && grid[x][y] == 1) {
								sum += 1;
								grid[x][y] = 0;
								stack.push(new Coordinate(x, y));
							}
						}
						x = c.x + 1;
						if (x < n) { // Right Hand Side
							y = c.y - 1;
							if (y >= 0 && grid[x][y] == 1) {
								sum += 1;
								grid[x][y] = 0;
								stack.push(new Coordinate(x, y));
							}
							y = c.y;
							if (grid[x][y] == 1) {
								sum += 1;
								grid[x][y] = 0;
								stack.push(new Coordinate(x, y));
							}
							y = c.y + 1;
							if (y < m && grid[x][y] == 1) {
								sum += 1;
								grid[x][y] = 0;
								stack.push(new Coordinate(x, y));
							}
						}
						x = c.x;
						y = c.y - 1;
						if (y >= 0 && grid[x][y] == 1) {
							sum += 1;
							grid[x][y] = 0;
							stack.push(new Coordinate(x, y));
						}
						y = c.y + 1;
						if (y < m && grid[x][y] == 1) {
							sum += 1;
							grid[x][y] = 0;
							stack.push(new Coordinate(x, y));
						}
					}
					max = Math.max(max, sum);
				}
			}
		}
	}
	
	public static class Coordinate {
		
		final int x, y;
		
		Coordinate(int x, int y) {
			this.x = x;
			this.y = y;
		}
	}
}
