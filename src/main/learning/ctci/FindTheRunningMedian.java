package learning.ctci;

import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Scanner;

public class FindTheRunningMedian {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		PriorityQueue<Integer> lowers = new PriorityQueue<Integer>(Comparator.reverseOrder());
		PriorityQueue<Integer> uppers = new PriorityQueue<Integer>();
		
		int n = sc.nextInt();
		for (int i = 0; i < n; i++) {
			int ai = sc.nextInt();
			
			if (!lowers.isEmpty() && ai <= lowers.peek()) { // If value <= median
				lowers.offer(ai);
			} else { // If beginning, or value > median
				uppers.offer(ai);
			}
			
			while (lowers.size() > uppers.size()) { // While lower heap tree(max) > upper heap tree(min)
				uppers.offer(lowers.poll());
			}
			while (uppers.size() - 1 > lowers.size()) { // While upper heap tree(min) - 1 > lower heap tree(max)
				lowers.offer(uppers.poll());
			}
			
			double median;
			if (lowers.size() == uppers.size()) { // If even, get min from upper heap tree and max from lower heap tree
				median = (lowers.peek() + uppers.peek()) / 2.0;
			} else { // If odd, get median from upper heap tree (ensured to always have the odd element)
				median = uppers.peek();
			}
			System.out.printf("%.1f\n", median);
		}
		
		sc.close();
	}
	
}
