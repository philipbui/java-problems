package learning.ctci;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class LonelyInteger {
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int a[] = new int[n];
		for(int a_i=0; a_i < n; a_i++){
			a[a_i] = in.nextInt();
		}
		Set<Integer> set = new HashSet(a.length);
		for (int i = 0; i < a.length; i++) {
			int v = a[i];
			if (set.contains(v)) {
				set.remove(v);
			} else {
				set.add(v);
			}
		}
		System.out.print(set.iterator().next());
	}
}
