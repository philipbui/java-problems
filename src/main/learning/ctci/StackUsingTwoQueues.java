package learning.ctci;

import java.util.Stack;

public class StackUsingTwoQueues {
	
	// If old is empty, tip new so it becomes FIFO.
	// Don't add to old if not empty, otherwise you will add something to the (oldestOnTop) from the (newestOnTop).
	// Instead, add when empty, since we don't need to pop/peek anything off the newest, we can always get the oldestOnTop
	// This ensures it will always be oldestOnTop when tipped.
	public static class MyQueue<T> {
		
		Stack<T> stackNewestOnTop = new Stack<T>();
		Stack<T> stackOldestOnTop = new Stack<T>();
		
		public void enqueue(T value) { // Push onto newest stack
			stackNewestOnTop.add(value);
		}
		
		public T peek() {
			tip();
			return stackOldestOnTop.peek();
		}
		
		public T dequeue() {
			tip();
			return stackOldestOnTop.pop();
		}
		
		public void tip() {
			if (stackOldestOnTop.isEmpty()) {
				while (!stackNewestOnTop.isEmpty()) {
					stackOldestOnTop.push(stackNewestOnTop.pop());
				}
			}
		}
	}
}
