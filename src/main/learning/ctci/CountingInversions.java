package learning.ctci;

import java.util.Arrays;

public class CountingInversions {
	
	public static void main(String[] args) {
		int[] test1 = new int[] {2,1,3,1,2};
		System.out.print(sort(test1, 0, test1.length - 1));
		int[] test2 = new int[] {1,1,1,2,2};
		
		System.out.print(sort(test2, 0, test2.length - 1));
	}
	
	/**
	 * Merges two sub-arrays of arr[].
	 * First subarray is arr[l..m]
	 * Second subarray is arr[m+1..r]
	 * @param arr
	 * @param l
	 * @param m
	 * @param r
	 * @return
	 */
	static long merge(int arr[], int l, int m, int r) {
		// Find sizes of two sub-arrays to be merged
		int n1 = m - l + 1; // Middle - (Elements to Left to starting L sub-array) + 1
		int n2 = r - m; // Right (Already ignores Elements to Right of ending R sub-array) - Middle.
		
		int L[] = Arrays.copyOfRange(arr, l, m + 1);
		int R[] = Arrays.copyOfRange(arr, m+1, r + 1);
		
		// Initial indexes of first and second sub-arrays
		int i = 0, j = 0;
		
		// Initial index of where to sort sub-arrays into original array. Starts from L-index and ends up at R-index.
		int k = l;
		long inversions = 0;
		while (i < n1 && j < n2) {
			if (L[i] <= R[j]) {
				arr[k] = L[i];
				i++;
			} else {
				arr[k] = R[j];
				j++;
				inversions += n1-i;
			}
			k++;
		}
		// One sub-array is empty from above while condition, just copy remaining elements from either sub-array.
		while (i < n1) { // Copy remaining L[] elements
			arr[k] = L[i];
			i++;
			k++;
			// No need to add inversions on left side since already on left
		}
		
		while (j < n2) { // Copy remaining R[] elements
			arr[k] = R[j];
			j++;
			k++;
			// No need to add inversions on right side since already on end
		}
		
		return inversions;
	}
	
	/**
	 * Merge Sort. 
	 * @param arr Entire array. Passed by value of reference so calls in subsequent stacks modify the array(through reference) in original stack.
	 * @param l Beginning index within arr to sort
	 * @param r End index within arr to sort
	 * @return Count of inversions required.
	 */
	private static long sort(int arr[], int l, int r) {
		long inversions = 0;
		if (l < r) {
			int m = (l + r) / 2; // Find the middle point
			
			// Sort first and second halves
			inversions += sort(arr, l, m);
			inversions += sort(arr, m + 1, r);
			
			// Merge the sorted halves
			inversions += merge(arr, l, m, r);
		}
		return inversions;
	}
}
